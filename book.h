#ifndef BOOK_H
#define BOOK_H

#include <stdio.h>

#define TAMANHOCODIGO 8
#define TAMANHODATA 20

typedef enum { COMPRA, VENDA } TipoOferta;

typedef struct {
    char codigo[TAMANHOCODIGO];
    int quantia;
    double preco;
    TipoOferta tipoOferta;
} Oferta;

typedef struct node {
    struct node *proximo;
    Oferta *oferta;
} Node;

// representacao de lista
typedef struct {
    Node *inicio;
    int tamanho;
} Lista;

// representacao de um book, cada papel tem seu proprio book
typedef struct book {
    char codigo[TAMANHOCODIGO];
    Lista *ofertasCompra;
    Lista *ofertasVenda;
    struct book *proximo; // cada book eh um node da Bolsa
    double ultimaCotacao;
} Book;

Book *inicializaBook(FILE *arquivo);

void fechaBook(Book *book, FILE *arquivo);

void mostraBook(Book *book);

Oferta *criaOferta(char *codigo, TipoOferta tipoOferta, double preco, int quantia);

void insereOferta(Book *book, Oferta *novaOferta);

void removeOferta(Book *book, Oferta *oferta);

Oferta *buscaOferta(Book *book, char *codigo, TipoOferta tipoOferta);

void mostraOferta(Oferta *oferta);

int bookEstaVazio(Book *book);

Oferta *pegaOfertaPorIndex(Book *book);

void editaOferta(Book *book);

#endif  // BOOK_H
