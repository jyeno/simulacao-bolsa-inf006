#include "utils.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// assim como variaveis tem ponteiros, funcoes tambem tem, esse tipo de estrategia
// tem vantagem ate mesmo no C apesar de ser menos amigavel de se ler no comeco
// alem desse modo, voce tambem ter funcoes que recebem outra funcoes, vide a
// funcao da stdlib.h , `qsort` , um exercicio interessante onde voce pode praticar
// e aprender mais coisas eh https://brennan.io/2015/01/16/write-a-shell-in-c/
int (*validador[]) (char const *) = { &validaCodigo, &validaTipoOferta };

double
leNumero(char const *mensagem) {
    double num = 0;
    char strNum[8];
    while(0xBADC0FFEE) {
        printf("%s", mensagem);
        fgets(strNum, 8, stdin);
        limpaStdin(strNum);

        char *ultimoCaractere = NULL;
        num = strtod(strNum, &ultimoCaractere);
        // so queremos aceitar valores maiores que 0, e caso o usuario nao tenha excedido
        // o limite de caracteres enviados nem digitado caracteres nao numericos
        if (num > 0 && *ultimoCaractere == '\n') {
            break;
        }
    }
    return num;
}

char *
leString(char const *mensagem, Validacao validacao) {
    char *codigo = calloc(8, sizeof(char));
    if (!codigo) return NULL; // alocacao falhou, melhor retornar nulo e tratar em outro lugar

    // fazendo uso da estrategia dita acima, validacao (do jeito que ta atualmente)
    // tem dois valores possiveis, de 0 a 1 (inclusivo), desse modo validacao sempre
    // vai poder acessar o devido ponteiro de funcao adequado para o tipo de validacao
    // se codigo for valido saimos do loop
    while (!validador[validacao](codigo)) {
        printf("%s", mensagem);
        fgets(codigo, 8, stdin);
        limpaStdin(codigo); // garantindo que nao tem restos no dispositivo de entrada

        char *caractereQuebraDeLinha = strchr(codigo, '\n');
        if (caractereQuebraDeLinha) *caractereQuebraDeLinha = '\0';
        paraCaixaAlta(codigo);
    }

    return codigo;
}

void
paraCaixaAlta(char *str) {
    for (int i = 0; str[i]; i += 1) {
        // fazendo uso dos valores da tabela ASCII
        if (str[i] >= 'a' && str[i] <= 'z') str[i] -= 32;
    }
}

int
validaCodigo(char const *codigo) {
    int tamanhoStr = strlen(codigo); // if codigo for menor que 5 ou maior que 6, ele eh um codigo invalido
    if (tamanhoStr < 5 || tamanhoStr > 6) return 0;

    for (int i = 0; codigo[i] && i < 4; i += 1) {
        // checando se os 4 primeiros caracteres sao letras
        if (codigo[i] < 'A' || codigo[i] > 'Z') return 0;
    }
    // checando se o quinto (e talvez sexto) caractere eh um numero (se nao for tipoPapel
    // sera 0 e consequentemente sera considerado como invalido), a numeracao do codigo
    // tem que estar entre 1 e 11 (inclusivo), nao deve haver 0s
    if (codigo[4] == '0' && codigo[5]) return 0; // atoi nao trata desse caso

    int tipoPapel = atoi(&codigo[4]);
    return tipoPapel >= 1 && tipoPapel <= 11 ? 1 : 0;
}

int
validaTipoOferta(char const *codigo) {
    return !strcmp(codigo, "COMPRA") || !strcmp(codigo, "VENDA");
}

void
limpaStdin(char *ultimaStrLida) {
    if (strchr(ultimaStrLida, '\n') == NULL) {
        int c;
        do {
            c = getchar();
        } while (c != '\n' && c != EOF);
    }
}

double
min(double a, double b) {
    return a >= b ? b : a;
}
