#include "bolsa.h"
#include "utils.h"

#include <stdlib.h>
#include <string.h>
#include <time.h>

char const *caminhoBolsaOrdens = "bolsaOrdens.data";
char const *caminhoOfertasConcluidas = "ordensConcluidas.data";

Bolsa *
inicializaBolsa() {
    Bolsa *bolsa = calloc(1,  sizeof(Bolsa));
    FILE *arquivo = fopen(caminhoBolsaOrdens, "r");
    if (arquivo) {
        while (!feof(arquivo)) {
            Book *book = inicializaBook(arquivo);
            insereBook(bolsa, book);
        }
        fclose(arquivo);
    }
    return bolsa;
}

void
insereBook(Bolsa *bolsa, Book *book) {
    if (!bolsa || !book) return; // nenhum dos dois valores deve ser nulo

    if (bolsa->tamanho == 0) {
        bolsa->inicio = book;
    } else {
        Book *node = bolsa->inicio;
        while (node->proximo) {
            node = node->proximo;
        }
        node->proximo = book;
    }
    bolsa->tamanho += 1;
}

void
removeBook(Bolsa *bolsa, Book **book) {
    if (bolsa->tamanho == 0) return;

    Book *aDeletar = NULL;
    if (bolsa->tamanho == 1 || bolsa->inicio == *book) {
        aDeletar = bolsa->inicio;
        bolsa->inicio = aDeletar->proximo;
    } else {
        Book *node = bolsa->inicio;
        while (node->proximo && node->proximo != *book) {
            node = node->proximo;
        }
        // se chegarmos ao final da lista e nao achamos o book que procuravamos
        // (node->proximo nulo), saimos da funcao sem fazer nada
        if (!node->proximo) return;

        aDeletar = node->proximo;
        node->proximo = aDeletar->proximo;
    }
    bolsa->tamanho -= 1;
    fechaBook(aDeletar, NULL);
}

void
tentaRemoverBook(Bolsa *bolsa, Book **book) {
    Lista *ofertasCompra = (*book)->ofertasCompra;
    Lista *ofertasVenda = (*book)->ofertasVenda;
    // somente removemos caso o book nao tenha ofertas de compra e venda
    if (ofertasCompra->tamanho > 0 || ofertasVenda->tamanho > 0) return;

    removeBook(bolsa, book);
}

Book *
buscaBook(Bolsa *bolsa, char const *codigo) {
    for (Book *book = bolsa->inicio; book; book = book->proximo) {
        if (!strcmp(book->codigo, codigo)) {
            return book;
        }
    }
    return NULL;
}

void
mostraTodosBooks(Bolsa *bolsa) {
    if (bolsa->tamanho > 0) {
        for (Book *node = bolsa->inicio; node; node = node->proximo) {
            mostraBook(node);
            printf("\n");
        }
    } else {
        printf("Sem books abertos!\n");
    }
}

void
fechaBolsa(Bolsa *bolsa) {
    FILE *arquivo = fopen(caminhoBolsaOrdens, "w");
    if (!arquivo) printf("Nao foi possivel abrir o arquivo para escrita, logo as mudancas NAO serao salvas.\n");

    Book *node = bolsa->inicio, *aLiberar = NULL;
    while (node) {
        aLiberar = node;
        node = node->proximo;
        fechaBook(aLiberar, arquivo);
    }
    if (arquivo) fclose(arquivo);
    free(bolsa);
}

void
mostraOfertasConcluidas() {
    FILE *arquivoOfertasConcluidas = fopen(caminhoOfertasConcluidas, "r");
    if (arquivoOfertasConcluidas) {
        char data[TAMANHODATA], codigo[TAMANHOCODIGO];
        double precoMedio = 0;
        int quantia = 0;
        while (fscanf(arquivoOfertasConcluidas, "%7[^:]: %lf %d %[^\n]\n", codigo, &precoMedio, &quantia, data) == 4) {
            double valorTotal = precoMedio * quantia;
            printf(" Papel: %s   Valor Total: %0.2lf   Preco Medio: %0.2lf   Quantidade: %d   %s\n", codigo, valorTotal, precoMedio, quantia, data);
        }
        fclose(arquivoOfertasConcluidas);
    } else {
        printf("Sem ofertas de compra e venda concluidas!\n");
    }
}

int
tentaConcluirOfertas(Book *book, TipoOferta ultimaOfertaTipo) {
    // somente podemos concluir a oferta quando ao menos temos uma de compra e uma de venda
    if (book->ofertasCompra->tamanho < 1 || book->ofertasVenda->tamanho < 1) return 0;

    double precoMedio = 0;
    int quantidade = 0;
    { // exemplo de escopo, o que tiver na stack desse escopo sera liberado ao seu final, util para mostrar ate onde variaveis serao usadas
        Node *nodeCompras = book->ofertasCompra->inicio;
        Node *nodeVendas = book->ofertasVenda->inicio;

        // enquanto nodeVendas E nodeCompras nao forem nulos E o preco do topo das ofertas de compra for maior que o topo das ofertas de venda
        while (nodeVendas && nodeCompras && nodeCompras->oferta->preco >= nodeVendas->oferta->preco) {
            Oferta *ofertaCompra = nodeCompras->oferta;
            Oferta *ofertaVenda = nodeVendas->oferta;

            // queremos pegar a menor quantia, para garantir que nao teremos nenhum numero negativo quando subtrairmos
            // observe que o menor preço de compra, no minimo deve ser igual ao de venda
            int quantiaMinima = min(ofertaVenda->quantia, ofertaCompra->quantia);
            quantidade += quantiaMinima;
            if (ultimaOfertaTipo == COMPRA) {
                precoMedio += ofertaVenda->preco * quantiaMinima;
                // pegando a cotacao do menor preco, nesse caso o de venda
                book->ultimaCotacao = ofertaVenda->preco;
            } else {
                precoMedio += ofertaCompra->preco * quantiaMinima;
                // pegando a cotacao do menor preco, nesse caso o de compra
                book->ultimaCotacao = ofertaCompra->preco;
            }

            ofertaVenda->quantia -= quantiaMinima;
            ofertaCompra->quantia -= quantiaMinima;

            // nao da para ser menor que 0, e caso qualquer uma das ofertas tiverem quantia
            // igual a 0, eles serao removidos de suas listas
            if (ofertaVenda->quantia == 0) {
                nodeVendas = nodeVendas->proximo;
                removeOferta(book, ofertaVenda);
            }
            if (ofertaCompra->quantia == 0) {
                nodeCompras = nodeCompras->proximo;
                removeOferta(book, ofertaCompra);
            }
        }
    }
    if (quantidade == 0) return 0; // nada a fazer, nao houve venda devido ao preco ser incompativel

    time_t now = time(NULL); // pegando o segundo mais recente
    struct tm *ts = localtime(&now); // convertendo para a struct de calendario e horario
    char data[TAMANHODATA];
    // formatando a data atual e a colocando na string, exemplo: "07:40 11/06/21 sex"
    // `man time` no terminal do linux, ou web, vai trazer as strings de formatacao (que
    // comecam com %) possiveis
    strftime(data, TAMANHODATA, "%H:%M %d/%m/%y %a", ts);

    double valorTotal = precoMedio; // nesse momento o precoMedio ainda representa o valor total de compra
    precoMedio /= quantidade; // dividindo para conseguir a media ponderada

    FILE *arquivoOfertasLog = fopen(caminhoOfertasConcluidas, "a");
    if (arquivoOfertasLog) {
        fprintf(arquivoOfertasLog, "%s: %lf %d %s\n", book->codigo, precoMedio, quantidade, data);
        fclose(arquivoOfertasLog);
    } else {
        printf("Erro: Ofertas concluidas NAO serao salvas devido a problemas na abertura/criacao do arquivo de log.\n");
    }

    fprintf(stdout, "Oferta concluida:\n Papel: %s   Valor Total: %0.2lf   Preco Medio: %0.2lf   Quantidade: %d   %s\n", book->codigo, valorTotal, precoMedio, quantidade, data);
    return 1;
}

void
limpaOfertasConcluidas() {
    // se remove retornar 0, entao houve sucesso em remover o arquivo
    if (remove(caminhoOfertasConcluidas) == 0) {
        printf("Historico de ofertas concluidas limpado com sucesso!\n");
    } else {
        printf("Erro: NAO foi possivel limpar as ofertas concluidas,\n"
               "voce nao tem ofertas concluidas ou falta permissao.\n");
    }
}

void
listaBooks(Bolsa *bolsa) {
    if (bolsa->tamanho == 0) return;

    printf("books com ofertas abertas:\n");
    for (Book *book = bolsa->inicio; book; book = book->proximo) {
        printf(" %s", book->codigo);
        if (book->ofertasCompra->tamanho > 0) {
            printf("  compra: %0.2lf", book->ofertasCompra->inicio->oferta->preco);
        }
        if (book->ofertasVenda->tamanho > 0) {
            printf("  venda: %0.2lf", book->ofertasVenda->inicio->oferta->preco);
        }
        if (book->ultimaCotacao > 0) {
            printf("  ultima cotacao: %0.2lf", book->ultimaCotacao);
        }
        printf("\n");
    }
    printf("\n");
}

Book *
numeraOfertas(Bolsa *bolsa) {
    if (bolsa->tamanho  == 0) {
        printf("Sem ofertas!\n");
        return NULL;
    }
    listaBooks(bolsa);

    char *codigo = leString("Book voce deseja ver as ofertas: ", CODIGO);
    Book *book = buscaBook(bolsa, codigo);
    if (book) { // o book existente sempre tera ao menos uma oferta
        Lista *arrayOfertas[] = { book->ofertasCompra, book->ofertasVenda, NULL };
        int i = 0, j = 1;
        for (Lista *ofertas = arrayOfertas[i]; ofertas; i += 1, ofertas = arrayOfertas[i]) {
            for (Node *node = ofertas->inicio; node; j += 1, node = node->proximo) {
                Oferta *oferta = node->oferta;
                printf("%d) %s %lf %d %s\n", j, oferta->codigo, oferta->preco, oferta->quantia, oferta->tipoOferta == COMPRA ? "COMPRA" : "VENDA");
            }
        }
    }

    free(codigo);
    return book;
}

Book *
pegaBookPorIndex(Bolsa *bolsa) {
    if (bolsa->tamanho == 0) return NULL;

    printf("books com ofertas abertas:\n");
    int i = 1;
    for (Book *book = bolsa->inicio; book; i += 1, book = book->proximo) {
        printf("%d) %s\n", i, book->codigo);
    }
    while (0xBADC0FFEE) {
        // a cada iteracao o valor retorna para o seu valor inicial
        int bookId = leNumero("Id do book: ");
        int j = 1;
        for (Book *node = bolsa->inicio; node; j += 1, node = node->proximo) {
            if (j == bookId) return node;
        }
    }
}
