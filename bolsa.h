#ifndef BOLSA_H
#define BOLSA_H

#include "book.h"

// implementando outra lista, esta contem books, e por discernimento eh chamada de Bolsa
typedef struct {
    Book *inicio;
    int tamanho;
} Bolsa;

Bolsa *inicializaBolsa();

void insereBook(Bolsa *bolsa, Book *book);

void removeBook(Bolsa *bolsa, Book **book);

void tentaRemoverBook(Bolsa *bolsa, Book **book);

Book *buscaBook(Bolsa *bolsa, char const *codigo);

void mostraTodosBooks(Bolsa *bolsa);

void fechaBolsa(Bolsa *bolsa);

int tentaConcluirOfertas(Book *book, TipoOferta ultimaOfertaTipo);

void mostraOfertasConcluidas();

void limpaOfertasConcluidas();

void listaBooks(Bolsa *bolsa);

Book *numeraOfertas(Bolsa *bolsa);

Book *pegaBookPorIndex(Bolsa *bolsa);

#endif // BOLSA_H
