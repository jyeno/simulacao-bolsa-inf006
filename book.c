#include "book.h"
#include "utils.h"

#include <stdlib.h>
#include <string.h>

Book *
inicializaBook(FILE *arquivo) {
    Book *book = calloc(1, sizeof(Book)); // calloc eh equivalente ao malloc, com a diferença que ele zera os membros da struct/array
    book->ofertasCompra = calloc(1, sizeof(Lista));
    book->ofertasVenda = calloc(1, sizeof(Lista));
    char codigo[TAMANHOCODIGO];
    if (arquivo && fscanf(arquivo, "%7s BOOK\n", codigo) == 1) {
        strncat(book->codigo, codigo, TAMANHOCODIGO - 1);

        double preco = 0;
        int quantia = 0;
        char tipoOfertaStr[8];
        while(fscanf(arquivo, " %d %lf %7s\n", &quantia, &preco, tipoOfertaStr) == 3) {
            TipoOferta tipoOferta = tipoOfertaStr[0] == 'C' ? COMPRA : VENDA;
            Oferta *oferta = criaOferta(codigo, tipoOferta, preco, quantia);
            insereOferta(book, oferta);
        }
        fscanf(arquivo, "\n"); // descartando essa quebra de linha, ja que ela nao nos interessa
    }
    return book;
}

void
fechaBook(Book *book, FILE *arquivo) {
    if (bookEstaVazio(book)) {
        goto libera_book; // se entrar aqui, nao ha o que escrever, o book esta vazio, logo vamos para a secao de liberar memoria
    } else {
        if (arquivo) fprintf(arquivo, "%s BOOK\n", book->codigo);
    }

    Lista *ofertas[] = { book->ofertasCompra, book->ofertasVenda, NULL }; // fazendo array para iterar e evitar codigo repetido
    for (int i = 0; ofertas[i]; i += 1) {
        if (ofertas[i]->tamanho < 1) continue; // caso tenha menos que 1 item, ela esta vazia, logo nao devemos fazer nada

        Node *node = ofertas[i]->inicio;
        Node *aLiberar = NULL;
        char const *tipoOfertaStr = node->oferta->tipoOferta == COMPRA ? "COMPRA" : "VENDA";
        while (node) {
            if (arquivo) { // se nao houve sucesso em abrir o arquivo, nao queremos tentar escrever nada nele, logo verificamos se ele eh valido
                fprintf(arquivo, " %d %lf %s\n", node->oferta->quantia, node->oferta->preco, tipoOfertaStr);
            }
            aLiberar = node;
            node = aLiberar->proximo;
            free(aLiberar->oferta);
            free(aLiberar);
        }
    }
    if (arquivo) fprintf(arquivo, "\n");
// area rotulada do codigo, possibilita ir para ela ao inves de executar as linhas sequencialmente
libera_book:
    free(book->ofertasCompra);
    free(book->ofertasVenda);
    free(book);
}

void
mostraBook(Book *book) {
    if (!book) return; // se o book for invalido, nao queremos fazer nada com ele

    printf("--------------- %s BOOK ------------------\n", book->codigo);
    if (book->ultimaCotacao > 0) printf("           Ultima Cotacao: %.2lf\n", book->ultimaCotacao);
    printf("       Compra        |        Venda\n");
    printf(" preco   quantidade  |  preco   quantidade\n");

    Node *nodeCompras = book->ofertasCompra->inicio;
    Node *nodeVendas = book->ofertasVenda->inicio;
    while (nodeCompras || nodeVendas) {
        if (nodeCompras) {
            mostraOferta(nodeCompras->oferta);
            nodeCompras = nodeCompras->proximo;
        }
        if (nodeVendas) {
            mostraOferta(nodeVendas->oferta);
            nodeVendas = nodeVendas->proximo;
        }
        printf("\n");
    }
}

Oferta *
criaOferta(char *codigo, TipoOferta tipoOferta, double preco, int quantia) {
    Oferta *oferta = calloc(1, sizeof(Oferta));
    oferta->tipoOferta = tipoOferta;
    oferta->preco = preco;
    oferta->quantia = quantia;
    // garantindo que a string vai ser terminada com '\0',
    // strcpy e strncpy nao garantem que a string vai ser terminada com '\0'
    strncat(oferta->codigo, codigo, TAMANHOCODIGO - 1);
    return oferta;
}

void
insereOferta(Book *book, Oferta *novaOferta) {
    if (!novaOferta) return; // nada a remover, logo saimos da funcao

    Node *novoNode = calloc(1, sizeof(Node));
    novoNode->oferta = novaOferta;

    int ehOfertaCompra = novaOferta->tipoOferta == COMPRA;
    Lista *ofertas = ehOfertaCompra ? book->ofertasCompra : book->ofertasVenda;

    // se tivermos tamanho 0 entao a lista esta vazia
    if (ofertas->tamanho == 0) {
        ofertas->inicio = novoNode;
        strncpy(book->codigo, novaOferta->codigo, TAMANHOCODIGO);
    } else {
        Node *node = ofertas->inicio;

        // caso o primeiro node nao for o que procuramos, vamos iterar
        // e buscar a posicao nos baseando no preco de cada oferta, exemplo:
        // simulacao de compra
        // (22 > 23) == 1   (nao entra no if, 23 deve se tornar o novo topo)
        // (24 > 12) == 1   (entra no if, itera com essa logica ate que ache a devida posicao)
        //
        // simulacao de venda
        // (10 > 9) == 0   (nao entra no if, 9 deve se tornar o novo topo)
        // (10 > 12) == 0  (entra no if, itera com essa logica ate que ache a devida posicao)
        if ((node->oferta->preco > novaOferta->preco) == ehOfertaCompra) {
            // paramos se node->proximo for nulo ou devido a logica acima (posicao encontrada)
            while (node->proximo && (node->proximo->oferta->preco > novaOferta->preco) == ehOfertaCompra) {
                node = node->proximo;
            }
            // Reposicionando os nodes de lugar
            novoNode->proximo = node->proximo;
            node->proximo = novoNode;
        } else {
            // se entrar aqui entao a nova oferta deva ficar no topo
            novoNode->proximo = ofertas->inicio;
            ofertas->inicio = novoNode;
        }
    }
    ofertas->tamanho += 1;
}

void
removeOferta(Book *book, Oferta *oferta) {
    if (!oferta) return; // nada a remover, logo saimos da funcao

    Lista *ofertas = (oferta->tipoOferta == COMPRA) ?
                      book->ofertasCompra :
                      book->ofertasVenda;

    Node *aDeletar = NULL;
    if (ofertas->inicio->oferta == oferta) {
        aDeletar = ofertas->inicio;
        ofertas->inicio = aDeletar->proximo;
    } else {
        Node *node = ofertas->inicio;
        while (node->proximo && node->proximo->oferta != oferta) {
            node = node->proximo;
        }
        if (!node->proximo) {
            return; // caso entre aqui, nao temos o que remover, logo saimos
        }
        aDeletar = node->proximo;
        node->proximo = aDeletar->proximo;
    }
    ofertas->tamanho -= 1;
    free(aDeletar->oferta);
    free(aDeletar);
}

Oferta *
buscaOferta(Book *book, char *codigo, TipoOferta tipoOferta) {
    Lista *ofertas = (tipoOferta == COMPRA) ? book->ofertasCompra :
                                              book->ofertasVenda;

    for (Node *node = ofertas->inicio; node; node = node->proximo) {
        if (strcmp(node->oferta->codigo, codigo)) {
            return node->oferta;
        }
    }
    return NULL;
}

void
mostraOferta(Oferta *oferta) {
    if (!oferta) return;

    // fazendo uso de codigo de escape ANSI,
    // https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797
    if (oferta->tipoOferta == VENDA) {
        printf("\033[25G"); // movendo o cursor para a coluna 25 da linha atual
    } else {
        printf("\033[2G"); // movendo o cursor para a coluna 2 da linha atual
    }
    printf("%0.2lf     %i", oferta->preco, oferta->quantia);
    printf("\033[0G"); // resetando o cursor para a primeira posicao
}

int
bookEstaVazio(Book *book) {
    return book->ofertasCompra->tamanho == 0 &&
           book->ofertasVenda->tamanho == 0;
}

Oferta *
pegaOfertaPorIndex(Book *book) {
    int ofertaId = 0;
    Lista *arrayOfertas[] = { book->ofertasCompra, book->ofertasVenda, NULL };
    while (0xBADC0FFEE) {
        ofertaId = leNumero("Id da oferta: ");
        int i = 0, j = 1; // a cada iteracao os valores retornam para o seu valor inicial
        for (Lista *ofertas = arrayOfertas[i]; ofertas; i += 1, ofertas = arrayOfertas[i]) {
            for (Node *node = ofertas->inicio; node; j += 1, node = node->proximo) {
                if (j == ofertaId) return node->oferta;
            }
        }
    }
}

void
editaOferta(Book *book) {
    Oferta *oferta = pegaOfertaPorIndex(book);
    // verificando se o ultimo caractere eh o que representa a acao fracionaria ou nao
    int ehFracionario = book->codigo[strlen(book->codigo) - 1] == 'F';
    double preco = leNumero("Preco: ");
    int invalido = 1, quantia  = 0;
    do {
        quantia = leNumero("Quantidade: ");
        if (ehFracionario && quantia <= 99) invalido = 0;
        else if(!ehFracionario && quantia % 100 == 0) invalido = 0;
    } while (invalido);

    // adicionando uma nova oferta com os valores desejados
    insereOferta(book, criaOferta(oferta->codigo, oferta->tipoOferta, preco, quantia));
    // removendo a antiga oferta
    removeOferta(book, oferta);
}
