#ifndef UTILS_H
#define UTILS_H

typedef enum { CODIGO, TIPO_OFERTA } Validacao;

double leNumero(char const *mensagem);

char *leString(char const *mensagem, Validacao validacao);

void limpaStdin(char *ultimaStrLida);

double min(double a, double b);

void paraCaixaAlta(char *str);

int validaCodigo(char const *codigo);

int validaTipoOferta(char const *tipoOfertaStr);

#endif // UTILS_H
