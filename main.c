#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "book.h"
#include "bolsa.h"
#include "utils.h"

void
fazOferta(Bolsa *bolsa) {
    listaBooks(bolsa);

    char *codigo = leString("codigo (LLLLN | LLLLNF | LLLLNN): ", CODIGO); // L == letra   N == numero  F == Fracionario
    // verificando se o ultimo caractere eh o que representa a acao fracionaria ou nao
    int ehFracionario = codigo[strlen(codigo) - 1] == 'F';

    Book *book = buscaBook(bolsa, codigo);
    if (!book) { // caso o book ainda nao existir, vamos cria-lo
        book = inicializaBook(NULL); // nao queremos ler de nenhum arquivo
        insereBook(bolsa, book);
    }

    char *tipoOfertaStr = leString("Tipo de oferta (compra | venda): ", TIPO_OFERTA);
    TipoOferta tipoOferta = tipoOfertaStr[0] == 'C' ? COMPRA : VENDA;

    double preco = leNumero("Preco: ");
    int quantia  = 0;
    while (0xBADC0FFEE) { // faz loop ate que seja valida a quantia solicitada pelo usuario
        quantia = leNumero("Quantidade: ");
        if (ehFracionario && quantia <= 99) break;
        if(!ehFracionario && quantia % 100 == 0) break;
    }

    Oferta *oferta = criaOferta(codigo, tipoOferta, preco, quantia);

    insereOferta(book, oferta);

    tentaConcluirOfertas(book, tipoOferta); // sempre vamos tentar concluir ofertas criadas
    tentaRemoverBook(bolsa, &book);         // e tambem remover o book, caso esteja vazio

    free(codigo);
    free(tipoOfertaStr);
}

void
menuAvancado(Bolsa *bolsa) {
    int opcao = 0;
    while (opcao != 5) {
        opcao = leNumero("\n1) Editar oferta   2) Remover oferta   3) Remover book   4) Limpar operacoes concluidas   5) Voltar\n");

        if (opcao == 1) {
            Book *book = numeraOfertas(bolsa); // retornando o book cujo foram numeradas as ofertas
            if (book) {
                editaOferta(book);
                printf("Oferta editada!\n");
            } else {
                printf("Nao temos nenhuma oferta no momento pro book escolhido!\n");
            }
        } else if (opcao == 2) {
            Book *book = numeraOfertas(bolsa); // retornando o book cujo foram numeradas as ofertas
            if (book) {
                Oferta *oferta = pegaOfertaPorIndex(book);
                removeOferta(book, oferta);
                printf("Oferta removida!\n");
            } else {
                printf("Nao temos nenhuma oferta no momento pro book escolhido!\n");
            }
        } else if (opcao == 3) {
            Book *book = pegaBookPorIndex(bolsa);
            if (book) {
                removeBook(bolsa, &book);
                printf("Book removido!\n");
            } else {
                printf("Sem books com ofertas!\n");
            }
        } else if (opcao == 4) {
            limpaOfertasConcluidas();
        } else if (opcao == 5) {
            printf("Voltando ao menu anterior...\n");
        } else {
            printf("Entrada invalida.\n");
        }
    }
}

void
help() {
    printf("Modo de usar:\n"
           "Linux\n"
           "./bolsa           (modo interativo)\n"
           "Windows\n"
           "bolsa             (modo interativo)\n"
           "Onde \"bolsa\" eh o binario compilado.\n");
}

int
main(int argc, char *argv[]) {
    if (argc != 1) {
        printf("Parametros nao suportados: ");
        for (int i = 1; argv[i]; i += 1) printf("%s ", argv[i]);
        printf("\n");
        help();
        exit(EXIT_FAILURE);
    }
    Bolsa *bolsa = inicializaBolsa();

    printf("==== Bem vindo a simulacao da bolsa ====\n\n");
    mostraTodosBooks(bolsa);

    // lembretes de uso do programa
    printf("Lembretes:\n"
           "Na realizacao de ofertas (primeira opcao), N significa numero, L significa letra e | eh um separador entre formatos validos\n"
           "Ofertas fracionarias (terminadas em F) devem ser menores ou iguais a 99.\n"
           "Ofertas nao fracionarias devem ser multiplas de 100.\n"
           "O modo \"avancado\" oferece um maior controle sob as ofertas/books existentes, cuidado.\n");

    int opcao = 0;
    while (opcao != 6) {
        opcao = leNumero("\n1) Fazer oferta   2) Mostrar ofertas   3) Mostrar todas as ofertas   4) Mostrar operacoes concluidas   5) Avancado   6) Sair\n");

        if (opcao == 1) {
            fazOferta(bolsa);
        } else if (opcao == 2) {
            if (bolsa->tamanho  == 0) continue;
            listaBooks(bolsa);

            char *codigo = leString("Book voce deseja ver as ofertas: ", CODIGO);
            Book *book = buscaBook(bolsa, codigo);
            if (book) {
                mostraBook(book);
            } else {
                printf("Nao temos nenhuma oferta no momento pro book %s !\n", codigo);
            }

            free(codigo);
        } else if (opcao == 3) {
            mostraTodosBooks(bolsa);
        } else if (opcao == 4) {
            mostraOfertasConcluidas();
        } else if (opcao == 5) {
            menuAvancado(bolsa);
        } else if (opcao == 6) {
            printf("Fechando a bolsa...\n");
            fechaBolsa(bolsa);
        } else {
            printf("Entrada invalida.\n");
        }
    }
}
