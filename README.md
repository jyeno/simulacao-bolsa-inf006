# Simulador da Bolsa de Valores

Este projeto tem como intuito servir como atividade prática avaliativa final da INF006 - Algoritmos e Estruturas de Dados do IFBA, além de também ser uma referência prática de algumas funcionalidades da linguagem de programação C.

## Recursos

Playlist recomendada de C: [Programando em C no Linux (inglês)](https://www.youtube.com/playlist?list=PLypxmOPCOkHXbJhUgjRaV2pD9MJkIArhg) (também válida de se ver quando ta no Windows, só tomar cuidado quando for especificidades do Linux)

Documentação de C: [cppreference](https://en.cppreference.com/w/c)

Grupo de C (e C++): [C/C++ Brasil](https://t.me/cppbrazil)

Repositorio com materiais de aprendizado: [material de aprendizado do C/C++ Brasil](https://github.com/cppbrasil/material-de-aprendizado)


### Como compilar e dicas

Este projeto foi testado com GCC (GNU Compiler Collection), CLANG (LLVM), TCC (Tiny C Compiler) e ZIG CC (zig C Compiler).
Para o GCC e CLANG foram usadas as seguintes flags (argumentos) na linha de comando:

 `gcc -g -fsanitize=address -Wall -Wextra -Wpedantic -Wwrite-strings -Werror -o simulacao_bolsa main.c utils.c book.c bolsa.c`

Para usar o CLANG basta substituir o `gcc` por `clang` do comando acima.
Para executar o programa basta executar `./simulacao_bolsa` no Linux e `simulacao_bolsa` no terminal do Windows.

Uma breve explicação das flags usadas:

<pre>
 -g                    // habilita o modo debug, útil quando se usa um debugador como o GDB (mais sobre ele abaixo)

 -fsanitize=address    // habilita o sanitizer (verificador/garantidor) de memoria (address) útil para checar vazamentos e acessos inválidos de memória

 -Wall                 // habilita vários warnings (avisos) recomendados

 -Wextra               // habilita alguns warnings extras que o -Wall nao habilita

 -Wpedantic            // habilita warnings de uso de funcionalidades que não estão no conjunto de funcionalidades padronizadas do C

 -Wwrite-strings       // habilita alguns warnings de uso/tratamento indevido de strings

 -Werror               // transforma warnings em erros de compilação, útil para garantir o tratamento de todos os warnings, principalmente quando é usado desde o inicio do projeto

 -o nome_do_programa   // cria o binário final com o nome de arquivo nome_do_programa
</pre>


É possível usar essas flags individualmente ou retirar alguma que não deseje no momento, como por exemplo `-Werror` e `-fsanitize=address`.

E vale lembrar, você somente deve colocar na linha de comando os arquivos de código fonte C (.c).

Arquivos de cabeçalho (header, .h) NÃO precisam nem devem ser colocados no comando.

para uma documentação completa, veja a manpage (página de manual) do gcc e/ou clang no seu sistema UNIX-like ou online.

### Debugar o codigo

O GDB (GNU DeBugger) é muito útil para descobrir a causa de bugs, dicas de material:

 - manpage do gdb (disponível nos sistemas UNIX-like com o gdb instalado, e online)

 - [Vídeo de exemplo e explicação de uso do GDB (inglês)](https://www.youtube.com/watch?v=rlN3XI8kuhI&list=PLypxmOPCOkHXbJhUgjRaV2pD9MJkIArhg&index=56)

 - [Post do stack overflow com dicas do GDB (inglês)](https://stackoverflow.com/questions/1471226/most-tricky-useful-commands-for-gdb-debugger)

## Requisitos

- Compilador C (pode ser qualquer um dos citados acima por exemplo)
- Terminal com suporte a caracteres de escape ANSI

---

É encorajado a leitura e sugestões de melhorias do código, não hesite em abrir uma issue ou MR (Merge Request :)

Você pode me contactar pelo [telegram](https://t.me/jyeno) para tirar dúvidas ou fazer sugestões também.
